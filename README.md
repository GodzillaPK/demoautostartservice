## Project Setup - Create a JAR file from source code

1. Clone the repository
2. Enetr into repository and run the command
```sh   
   mvn clean install package
```
3. JAR will be created in target directory of project folder

## Create Ubuntu Service

1. Open Project Folder --> ubuntu_service Directory
2. Make the necessary changes in the demo.service file as given below
```sh
[Unit]
Description=<Service Decription>
After=network.target

[Service]
Type=simple
User=root

Environment="port=8091"
Environment="standardOutput=< log file location for output file >" 
Environment="standardError=< log file location for error file >"

ExecStart=/bin/sh -c "/usr/bin/java -Dserver.port=${port} -jar -Xms128m -Xmx512m < JAR file name with location ex. /home/ubuntu/Downloads/demo/target/demo-0.0.1-SNAPSHOT.jar > >>${standardOutput} 2>>${standardError}"

Restart=always

[Install]
WantedBy=multi-user.target
```
3. Paste this changed service file in /etc/systemd/system path

## Enable the service

1. To releoad the service daemon use command 
```sh   
sudo systemctl daemon-reload
```
2. To Enable Service use command
```sh   
sudo systemctl enable demo.service
```
3. Start The Service
```sh   
sudo systemctl start demo.service
```

